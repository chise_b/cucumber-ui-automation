Feature: Show recently viewed products

  Scenario: When a customer view the details of a product, when they return, the viewed product will be displayed in the list of recently viewed products
    Given Customer is on product page
    When Customer returns to products page
    Then Customer should see the product in the recently viewed products list