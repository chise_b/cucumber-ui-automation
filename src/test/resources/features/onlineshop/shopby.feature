Feature: VIP Shop By Functionality
  This will bring up vip products filter by type

  Scenario: When a customer filters vip products by type, the filter will appear type: chosen type

    Given VIP ShopyBy Customer is on the vip page
    When VIP ShopBy Customer filters products by '<term>'
    Then VIP ShopBy Customer should see that the products are filtered by given '<term>'

    Examples:
      | term |
      | Denim |
      | Knits |