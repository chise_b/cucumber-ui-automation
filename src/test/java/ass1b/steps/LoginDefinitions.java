package ass1b.steps;

import ass1b.app.steps.serenity.LoginSteps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class LoginDefinitions {

    private static final String EMAIL = "chise_b@yahoo.com";
    private static final String PASSWORD = "TestEvozon";

    @Steps
    LoginSteps loginSteps;

    @Given("A user enters his email")
    public void user_enters_email() {
        loginSteps.is_the_login_page();
        loginSteps.enter_email(EMAIL);
    }

    @Given("A user enteres his password")
    public void user_enters_password() {
        loginSteps.enter_password(PASSWORD);
    }

    @When("He presses login")
    public void user_presses_login() {
        loginSteps.click_login();
    }

    @Then("He is logged in and welcomed with his name")
    public void check_user_is_login() {
        loginSteps.check_user_is_logged_in();
    }

}
