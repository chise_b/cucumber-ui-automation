package ass1b.steps;

import ass1b.app.steps.serenity.RecentlyViewedSteps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class RecentlyViewedDefinitions {
    @Steps
    RecentlyViewedSteps recentlyViewedSteps;

    @Given("Customer is on product page")
    public void customer_is_on_product_page() {
        recentlyViewedSteps.is_the_home_page();
        recentlyViewedSteps.click_women_menu();
        recentlyViewedSteps.click_tops_and_blouses_category();
        recentlyViewedSteps.click_tops_and_blouses_first_product();
    }

    @When("Customer returns to products page")
    public void customer_returns_to_product_page() {

        recentlyViewedSteps.go_back();
    }

    @Then("Customer should see the product in the recently viewed products list")
    public void customer_should_see_the_viewed_product_in_recently_viewed_items_list() {
        recentlyViewedSteps.check_recently_viewed_list_contains_first_product();
    }
}
