package ass1b.steps;

import ass1b.app.steps.serenity.VIPShopBySteps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class VIPShopByDefinitions {

    @Steps
    VIPShopBySteps vipShopBySteps;

    @Given("VIP ShopyBy Customer is on the vip page")
    public void vip_shopby_customer_is_on_the_main_page_of_http_qa1_dev_evozon_com() {
        vipShopBySteps.is_the_home_page();
        vipShopBySteps.click_vip_menu();
    }

    @When("VIP ShopBy Customer filters products by {string}")
    public void vip_shopby_customer_filters_products_by_type(String string){
        vipShopBySteps.filter_by_type(string);
    }

    @Then("VIP ShopBy Customer should see that the products are filtered by given {string}")
    public void vip_shopby_customer_should_see_that_the_products_are_filtered(String string){
        vipShopBySteps.products_filtered_by_given_type(string);
    }
}
