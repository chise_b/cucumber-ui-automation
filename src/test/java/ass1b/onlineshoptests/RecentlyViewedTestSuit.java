package ass1b.onlineshoptests;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/onlineshop/recentlyviewed.feature", glue="ass1b")
public class RecentlyViewedTestSuit {
}
