package ass1b.app.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://qa1.dev.evozon.com/customer/account/login/")
public class LoginPage extends PageObject {

    @FindBy(xpath = "//*[@id=\"email\"]")
    private WebElementFacade emailInput;

    @FindBy(xpath = "//*[@id=\"pass\"]")
    private WebElementFacade passwordInput;

    @FindBy(xpath = "/html/body/div/div/div[2]/div/div/div[2]/form/div/div[2]/div[2]/button")
    private WebElementFacade loginButton;

    @FindBy(xpath = "/html/body/div/div/div[2]/div/div[2]/div[2]/div/div[2]/p[1]/strong")
    private WebElementFacade welcomeTest;

    public void click_login_button(){
        loginButton.click();
    }

    public void enter_email(String email){
        emailInput.type(email);
    }

    public void enter_password(String password){
        passwordInput.type(password);
    }

    public WebElementFacade getWelcomeTest() {
        return welcomeTest;
    }
}
