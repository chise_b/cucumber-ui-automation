package ass1b.app.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

@DefaultUrl("http://qa1.dev.evozon.com/")
public class LandingPage extends PageObject {

    @FindBy(xpath = "//*[@id=\"nav\"]/ol/li[6]/a")
    private WebElementFacade vipMenu;

    @FindBy(xpath = "//*[@id=\"narrow-by-list\"]/dd[3]/ol")
    private WebElementFacade typeFilter;

    @FindBy(xpath = "//*[@id=\"top\"]/body/div/div/div[2]/div/div[2]/div[1]/div/div[2]/div[1]/ol/li/span[2]")
    private WebElementFacade filterValueSpan;

    @FindBy(xpath = "//*[@id=\"nav\"]/ol/li[1]/a")
    private WebElementFacade womenMenu;

    @FindBy(xpath = "//*[@id=\"top\"]/body/div/div/div[2]/div/div[2]/ul/li[2]/a")
    private WebElementFacade topsBlousesCategory;

    //@FindBy(xpath = "//*[@id=\"top\"]/body/div/div/div[2]/div/div[2]/div[1]/div[4]")
    @FindBy(className = "category-products")
    private WebElementFacade firstPageProductsTopBlouses;

    @FindBy(id = "recently-viewed-items")
    private WebElementFacade recentlyViewedItemsList;

    public void click_vip_menu() {
        vipMenu.click();
    }

    public void choose_type(String type) {
        List<WebElement> allTypes = typeFilter.findElements(By.tagName("li"));
        for (WebElement t : allTypes) {
            WebElement ahref = t.findElement(By.tagName("a"));
            String ahrefText = ahref.getText();
            if (ahrefText.substring(0, ahrefText.indexOf(" ")).equals(type)) {
                ahref.click();
                break;
            }
        }
    }

    public WebElementFacade getFilterValueSpan() {
        return filterValueSpan;
    }

    public void click_women_menu() {
        womenMenu.click();
    }

    public void click_tops_and_blouses_category() {
        topsBlousesCategory.click();
    }

    public void click_tops_and_blouses_first_product() {
        firstPageProductsTopBlouses.findElements(By.tagName("ul")).get(0)
                .findElements(By.tagName("li")).get(0)
                .findElement(By.tagName("a")).click();
    }

    public void go_back() {
        getDriver().navigate().back();
    }

    public List<String> getRecentlyViewedItemsList() {
        return recentlyViewedItemsList.findElements(By.className("product-name")).stream()
                .map(webElement -> webElement.findElement(By.tagName("a")))
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public String getFirstProductTopBlouses() {
        return firstPageProductsTopBlouses.findElements(By.tagName("li")).get(0).findElement(By.tagName("a")).getAttribute("title");
    }
}
