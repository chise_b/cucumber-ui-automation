package ass1b.app.steps.serenity;

import ass1b.app.pages.LoginPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class LoginSteps {

    LoginPage loginPage;
    private static final String LOGIN_WELCOME_MESSAGE = "Hello, Chise Eusebiu Bogdan!";

    @Step
    public void is_the_login_page() {
        loginPage.open();
    }

    @Step
    public void click_login() {
        loginPage.click_login_button();
    }

    @Step
    public void enter_email(String email) {
        loginPage.enter_email(email);
    }

    @Step
    public void enter_password(String password) {
        loginPage.enter_password(password);
    }

    @Step
    public void check_user_is_logged_in() {
        assertThat(loginPage.getWelcomeTest().getText(), equalTo(LOGIN_WELCOME_MESSAGE));
    }
}
