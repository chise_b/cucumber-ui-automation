package ass1b.app.steps.serenity;

import ass1b.app.pages.LandingPage;
import net.thucydides.core.annotations.Step;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class RecentlyViewedSteps {
    LandingPage landingPage;

    @Step
    public void is_the_home_page() {
        landingPage.open();
    }

    @Step
    public void click_women_menu() {
        landingPage.click_women_menu();
    }

    @Step
    public void click_tops_and_blouses_category() {
        landingPage.click_tops_and_blouses_category();
    }

    @Step
    public void click_tops_and_blouses_first_product() {
        landingPage.click_tops_and_blouses_first_product();
    }

    @Step
    public void go_back() {
        landingPage.go_back();
    }

    @Step
    public void check_recently_viewed_list_contains_first_product() {
        String firstProductName = landingPage.getFirstProductTopBlouses().toUpperCase();
        List<String> recentlyViewedItems = landingPage.getRecentlyViewedItemsList();

        assertEquals(1, recentlyViewedItems.size());
        assertEquals(firstProductName, recentlyViewedItems.get(0));
    }
}
