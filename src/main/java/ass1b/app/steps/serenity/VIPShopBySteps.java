package ass1b.app.steps.serenity;

import ass1b.app.pages.LandingPage;
import net.thucydides.core.annotations.Step;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class VIPShopBySteps {
    LandingPage landingPage;
    private static final String VIP = "VIP";

    @Step
    public void is_the_home_page() {
        landingPage.open();
    }

    @Step
    public void click_vip_menu() {
        landingPage.click_vip_menu();
    }

    @Step
    public void filter_by_type(String type){
        landingPage.choose_type(type);
    }

    @Step
    public void products_filtered_by_given_type(String type){
        assertThat(landingPage.getFilterValueSpan().getText(), equalTo(type));
    }
}
